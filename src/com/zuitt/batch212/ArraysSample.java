package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {

    public static void main(String[] args) {

//      Arrays = Fixed/limited collections of data
//        2^31 = 2,147,483,648 elements (capacity ng isang array)

//        Declaration
        int[] intArray = new int[3];

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;

        System.out.println(intArray[2]);

//        other syntax used to declare arrays
        int intSample[] = new int[2];

        intSample[0] = 50;

        System.out.println(intSample[0]);

//        String array
        String stringArray[] = new String[3];

        stringArray[0] = "John";
        stringArray[1] = "Jane";
        stringArray[2] = "Mina";

//        Declaration with initialization
        int[] intArray2 = {100, 200, 300, 400, 500};

        //System.out.println(intArray2); //memory location lang lalabas

        //convert to string representation
        System.out.println(Arrays.toString(intArray2));
        System.out.println(Arrays.toString(stringArray));

//        Methods used in arrays:

        //sort()
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

        //binarySearch(array, elementToBeSearch) - searches the specified array of the given data type for the Arrays.sort() method prior to making this call. If it is not sorted, the results are undefined. nagrereturn ng index
        String searchTerm = "Kelly";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);


//        Multidimensional arrays
        String[][] classroom = new String[3][3];

        //first row
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Nayeon";
        //second row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zorro";
        classroom[1][2] = "Sanji";
        //third row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classroom));


//        ArrayLists - resizable arrays, wherein elements can be added or removed whenever it is needed.

        //Declaration
        ArrayList<String> students = new ArrayList<>();

        //Adding elements
        students.add("John");
        students.add("Paul");

        //When using ArrayList, We dont need to use .toString() to print all the elements
        System.out.println(students);

        //Access elements to an ArrayList
        System.out.println(students.get(0));

        //updating an element
        students.set(1, "George");
        System.out.println(students);

        //Removing elements
        students.remove(1);
        System.out.println(students);

        //Removing all elements in an Arraylist using the clear()
        students.clear();
        System.out.println(students);

        //getting the number of elements in an ArrayList using size()
        //get the length of an array list
        System.out.println(students.size());


        //We can also declare and initialize values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);


//        Hashmaps -> key:value pair

        //Declaration
        HashMap<String, String> employeeRole = new HashMap<>();

        //Adding fields
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Nami");
        System.out.println(employeeRole);

        //Retrieving field value
        System.out.println(employeeRole.get("Captain"));

        //Removing elements
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        //Retrieving hashmap keys/fields
        System.out.println(employeeRole.keySet());

//        Hashmap with integers as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("English", 89);
        grades.put("Math", 93);
        System.out.println(grades);

//        Hashmap with Array list
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(75, 80, 90));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(89, 87, 85));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("Jane", gradesListB);

        System.out.println(subjectGrades);

    }
}
