package com.zuitt.batch212;

public class ControlStructs {

    public static void main(String[] args) {

//        Operators
        //1. Arithmetic = +, -, *, /, %
        //2. Comparison = >,<,>=,<=,==,!=
        //3. Assignment = =,+=,-=,*=,/=
        //4. Logical = &&, ||, !

//        Conditional statements
        int num1 = 10;
        int num2 = 20;

//        if statement
        if(num1 > 5)
            System.out.println("num1 is greater than 5");

//        if-else statement
        if(num2>100)
            System.out.println("num2 is greater than 100");
        else
            System.out.println("num2 is less than 100");

//        if-else if and else statement
        if(num1 == 5)
            System.out.println("num1 is equal to 5");
        else if (num2 == 20)
            System.out.println("num2 is equal to 20");
        else
            System.out.println("anything else");

//        short circuiting
//        & and | - logical operators (always evaluate both sides)
//        && and || - short circuit (it is not necessary to know what the right hand side is because the result can only be false regardless of the value) (example: false && ...)

        int x = 15;
        int y = 0;

//        System.out.println(x/y == 0);
        if(y>5 && x/y == 0)
            System.out.println("result is: "+x/y);
        else
            System.out.println("the condition has short circuited");

//        Switch statement - if the desired result is specific

        int directionValue = 4;

        switch (directionValue) {
            case 1:
                System.out.println("north");
                break;
            case 2:
                System.out.println("south");
                break;
            case 3:
                System.out.println("east");
                break;
            case 4:
                System.out.println("west");
                break;
            default:
                System.out.println("invalid");
        }
    }
}
