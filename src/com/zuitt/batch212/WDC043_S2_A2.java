package com.zuitt.batch212;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {

    public static void main(String[] args) {

        int[] primeNumbers = {2,3,5,7,11};

        System.out.println("The first prime number is: "+primeNumbers[0]);
        System.out.println("The second prime number is: "+primeNumbers[1]);
        System.out.println("The third prime number is: "+primeNumbers[2]);
        System.out.println("The fourth prime number is: "+primeNumbers[3]);
        System.out.println("The fifth prime number is: "+primeNumbers[4]);


        ArrayList<String> myFriends = new ArrayList<>();

        myFriends.add("John");
        myFriends.add("Jane");
        myFriends.add("Chloe");
        myFriends.add("Zoey");

        System.out.println("My friends are: "+ myFriends);


        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: "+inventory);

    }
}
